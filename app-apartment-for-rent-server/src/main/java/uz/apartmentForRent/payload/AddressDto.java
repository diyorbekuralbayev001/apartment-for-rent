package uz.apartmentForRent.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {

    private UUID id;

    private String name;

    private Double lat, lon;

    private DistrictDto districtDto;

    private boolean active;
}
