package uz.apartmentForRent.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.apartmentForRent.entity.enums.StatusType;
import uz.apartmentForRent.entity.enums.Timing;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ApartmentDto {

    private UUID id;

    private UUID userId;

    private UserDto userDto;

    private UUID addressId;

    private List<UUID> photoIds;

    private StatusType statusType;

    private Timing timing;

    private int countRoom;

    private Double sum;

    private UUID categoryId;

    private String description;

    private boolean active;

    public ApartmentDto(UserDto userDto, UUID addressId, List<UUID> photoIds, StatusType statusType, Timing timing, int countRoom, Double sum, UUID categoryId, String description) {
        this.userDto = userDto;
        this.addressId = addressId;
        this.photoIds = photoIds;
        this.statusType = statusType;
        this.timing = timing;
        this.countRoom = countRoom;
        this.sum = sum;
        this.categoryId = categoryId;
        this.description = description;
    }

    public ApartmentDto(UUID id, UUID userId, UUID addressId, List<UUID> photoIds, StatusType statusType, Timing timing, int countRoom, Double sum, UUID categoryId, String description, boolean active) {
        this.id = id;
        this.userId = userId;
        this.addressId = addressId;
        this.photoIds = photoIds;
        this.statusType = statusType;
        this.timing = timing;
        this.countRoom = countRoom;
        this.sum = sum;
        this.categoryId = categoryId;
        this.description = description;
        this.active = active;
    }

    public ApartmentDto(UUID id, UserDto userDto, UUID addressId, List<UUID> photoIds, StatusType statusType, Timing timing, int countRoom, Double sum, UUID categoryId, String description, boolean active) {
        this.id = id;
        this.userDto = userDto;
        this.addressId = addressId;
        this.photoIds = photoIds;
        this.statusType = statusType;
        this.timing = timing;
        this.countRoom = countRoom;
        this.sum = sum;
        this.categoryId = categoryId;
        this.description = description;
        this.active = active;
    }
}
