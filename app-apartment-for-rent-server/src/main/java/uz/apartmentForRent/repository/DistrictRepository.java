package uz.apartmentForRent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.apartmentForRent.entity.District;
import uz.apartmentForRent.projection.CustomDistrict;

import java.util.UUID;

@RepositoryRestResource(path = "district", collectionResourceRel = "list", excerptProjection = CustomDistrict.class)
public interface DistrictRepository extends JpaRepository<District, UUID> {
}
