package uz.apartmentForRent.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import uz.apartmentForRent.entity.Apartment;

import java.util.List;
import java.util.UUID;

public interface ApartmentRepository extends JpaRepository<Apartment, UUID> {

    List<Apartment> findByActiveTrue(Pageable pageable);

    Page<Apartment> findAllByUserId(Pageable pageable, UUID user_id);

}
