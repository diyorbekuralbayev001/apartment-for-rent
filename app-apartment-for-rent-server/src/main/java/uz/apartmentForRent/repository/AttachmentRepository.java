package uz.apartmentForRent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.apartmentForRent.entity.Attachment;
import uz.apartmentForRent.entity.Role;

import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
}
