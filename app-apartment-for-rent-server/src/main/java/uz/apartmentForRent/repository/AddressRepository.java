package uz.apartmentForRent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.apartmentForRent.entity.Address;

import java.util.List;
import java.util.UUID;

public interface AddressRepository extends JpaRepository<Address, UUID> {
    List<Address> findByLonBetweenAndLatBetween(Double lon, Double lon2, Double lat, Double lat2);

    List<Address> findByActiveTrue();
}
