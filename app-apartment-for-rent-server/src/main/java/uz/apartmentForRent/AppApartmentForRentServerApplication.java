package uz.apartmentForRent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppApartmentForRentServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppApartmentForRentServerApplication.class, args);
    }

}
