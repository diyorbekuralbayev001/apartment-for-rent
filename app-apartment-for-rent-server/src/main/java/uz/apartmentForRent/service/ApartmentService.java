package uz.apartmentForRent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import uz.apartmentForRent.entity.Apartment;
import uz.apartmentForRent.entity.Attachment;
import uz.apartmentForRent.entity.User;
import uz.apartmentForRent.entity.enums.StatusType;
import uz.apartmentForRent.payload.ApartmentDto;
import uz.apartmentForRent.payload.ApiResponse;
import uz.apartmentForRent.payload.UserDto;
import uz.apartmentForRent.repository.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ApartmentService {

    @Autowired
    ApartmentRepository apartmentRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;


    public ApiResponse saveOrEditApartment(ApartmentDto dto) {
        try {
            apartmentRepository.save(makeApartment(dto));
            return new ApiResponse("Successfully added", HttpStatus.OK.getReasonPhrase());
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.CONFLICT.getReasonPhrase());
        }
    }

    public Apartment makeApartment(ApartmentDto dto) {
        try {
            Apartment apartment = new Apartment();
            if (dto.getId() != null) {
                Optional<Apartment> optionalApartment = apartmentRepository.findById(dto.getId());
                if (optionalApartment.isPresent()) {
                    apartment = optionalApartment.get();
                }
            }
            apartment.setUser(userRepository.findById(dto.getUserDto().getId()).orElseThrow(() -> new ResourceNotFoundException("getUserID")));
            apartment.setAddress(addressRepository.findById(dto.getAddressId()).orElseThrow(() -> new ResourceNotFoundException("getAddressID")));
            apartment.setPhotos(attachmentRepository.findAllById(dto.getPhotoIds()));
            apartment.setStatusType(dto.getStatusType());
            apartment.setTiming(dto.getTiming());
            apartment.setCountRoom(dto.getCountRoom());
            apartment.setSum(dto.getSum());
            apartment.setCategory(categoryRepository.findById(dto.getCategoryId()).orElseThrow(() -> new ResourceNotFoundException("getCategoryID")));
            apartment.setDescription(dto.getDescription());
//            System.err.println(apartment.getUser().getRoles().stream().findFirst().get().getRoleName());
//            System.err.println(apartment.getUser().getRoles().stream().map(Role::getRoleName));
//            if (apartment.getUser().getRoles().stream().findFirst().get().getRoleName().name().equals("ROLE_SUPER_ADMIN") || apartment.getUser().getRoles().stream().findFirst().get().getRoleName().name().equals("ROLE_ADMIN"))
            apartment.setActive(dto.isActive());
            return apartment;
        } catch (Exception e) {
            return null;
        }
    }

    public ApiResponse getApartment(UUID id) {
        try {
            Apartment apartment = apartmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getApartmentID"));
            return new ApiResponse("Success", apartment);
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getAllApartment(int page, int size, User user) {
        try {
            Page<Apartment> apartments = apartmentRepository.findAll(PageRequest.of(page, size));
            if (user.getRoles().stream().findFirst().get().getRoleName().name().equals("ROLE_SUPER_ADMIN") ||
                    user.getRoles().stream().findFirst().get().getRoleName().name().equals("ROLE_ADMIN")) {
                return new ApiResponse("All apartment", apartments.getContent().stream().map(this::apartmentDto).collect(Collectors.toList()));
            }
            return new ApiResponse("All apartment active true for client", apartmentRepository.findByActiveTrue(PageRequest.of(page, size)).stream().map(this::apartmentDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getAllApartmentsForSeller(int page, int size, User user) {
        try {
            Page<Apartment> apartmentListSeller = apartmentRepository.findAllByUserId(PageRequest.of(page, size), user.getId());
            return new ApiResponse("this is seller  apartments", apartmentListSeller.stream().map(this::apartmentDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApartmentDto apartmentDto(Apartment apartment) {
        return new ApartmentDto(
                apartment.getId(),
//                 apartment.getUser().getId(),
                userDto(apartment.getUser()),
                apartment.getAddress().getId(),
                getAttachmentUUID(apartment.getPhotos()),
                apartment.getStatusType(),
                apartment.getTiming(),
                apartment.getCountRoom(),
                apartment.getSum(),
                apartment.getCategory().getId(),
                apartment.getDescription(),
                apartment.isActive()
        );
    }

    public UserDto userDto(User user) {
        return new UserDto(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getPhoneNumber(),
                user.getCompanyPhoneNumber()
        );
    }

    public List<UUID> getAttachmentUUID(List<Attachment> attachmentList) {
        List<UUID> uuidList = new ArrayList<>();
        for (Attachment attachment : attachmentList) {
            uuidList.add(attachment.getId());
        }
        return uuidList;
    }

    public ApiResponse deleteApartment(UUID id) {
        try {
            apartmentRepository.deleteById(id);
            return new ApiResponse("Deleted", HttpStatus.OK.getReasonPhrase());
        } catch (Exception e) {
            return new ApiResponse("Error for deleted", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse enabledApartment(UUID id) {
        try {
            jdbcTemplate.update("UPDATE apartment a set active = (" +
                    "    select CASE WHEN a2.active =true THEN false else true end status from apartment a2 where a2.id ='" + id + "'" +
                    "    ) where a.id='" + id + "'");
            Optional<Apartment> apartmentOptional = apartmentRepository.findById(id);
            if (apartmentOptional.isPresent()) {
                Apartment apartment = apartmentOptional.get();
                boolean active = !apartment.isActive();
                apartment.setStatusType(active ? StatusType.BUSY : StatusType.EMPTY);
                apartmentRepository.save(apartment);
                return new ApiResponse(active ? "Inactive" : "active", HttpStatus.OK.value());
            }
            return new ApiResponse("Apartment not found",
                    HttpStatus.CONFLICT.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }
}
