package uz.apartmentForRent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.apartmentForRent.entity.Attachment;
import uz.apartmentForRent.entity.AttachmentContent;
import uz.apartmentForRent.payload.ApiResponse;
import uz.apartmentForRent.repository.AttachmentContentRepository;
import uz.apartmentForRent.repository.AttachmentRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@Service
public class AttachmentService {
    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;


    /**
     * BU RASMNI YUKLASH UCHUN
     */
    public List<UUID> uploadFile(MultipartHttpServletRequest request) {
        try {
            Iterator<String> fileNames = request.getFileNames();
            List<UUID> uuidList=new ArrayList<>();
            while (fileNames.hasNext()) {
                MultipartFile file = request.getFile(fileNames.next());
                assert file != null;
                Attachment attachment = new Attachment(
                        file.getOriginalFilename(),
                        file.getSize(),
                        file.getContentType()
                );
                Attachment savedAttachment = attachmentRepository.save(attachment);

                AttachmentContent attachmentContent = new AttachmentContent(
                        savedAttachment,
                        file.getBytes());
                attachmentContentRepository.save(attachmentContent);
                uuidList.add(savedAttachment.getId());
            }
            return uuidList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public HttpEntity<?> getFile(UUID id) {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getAttachment"));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(id);
        return ResponseEntity.ok().contentType(MediaType.valueOf(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; fileName=\"" + attachment.getName() + "\"")
                .body(attachmentContent.getContent());
    }

    public ApiResponse deleteFile(UUID id) {
        try {
            attachmentContentRepository.deleteByAttachmentId(id);
            attachmentRepository.deleteById(id);
            return new ApiResponse("Deleted", HttpStatus.OK.value());
        }catch (Exception e){
            return new ApiResponse("Server error",HttpStatus.CONFLICT.value());
        }
    }

}
