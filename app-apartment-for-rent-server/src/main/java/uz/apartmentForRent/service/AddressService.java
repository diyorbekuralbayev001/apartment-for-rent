package uz.apartmentForRent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uz.apartmentForRent.entity.Address;
import uz.apartmentForRent.entity.District;
import uz.apartmentForRent.entity.Region;
import uz.apartmentForRent.payload.AddressDto;
import uz.apartmentForRent.payload.ApiResponse;
import uz.apartmentForRent.payload.DistrictDto;
import uz.apartmentForRent.payload.RegionDto;
import uz.apartmentForRent.repository.AddressRepository;
import uz.apartmentForRent.repository.DistrictRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AddressService {

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    AddressRepository addressRepository;

    public ApiResponse saveOrEditAddress(AddressDto dto) {
        try {
            addressRepository.save(makeAddress(dto));
            return new ApiResponse("Successfully added", HttpStatus.OK.getReasonPhrase());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public Address makeAddress(AddressDto addressDto) {
        Address address = new Address();
        if (addressDto.getId() != null) {
            Optional<Address> optionalAddress = addressRepository.findById(addressDto.getId());
            if (optionalAddress.isPresent()) {
                address = optionalAddress.get();
            }
        }
        address.setName(addressDto.getName());
        address.setLat(address.getLat());
        address.setLon(addressDto.getLon());
        address.setDistrict(districtRepository.findById(addressDto.getDistrictDto().getId()).orElseThrow(() -> new ResourceNotFoundException("getDistrictID")));
        address.setActive(addressDto.isActive());
        return address;
    }

    public ApiResponse getAllAddress(int page, int size) {
        Page<Address> addressPage = addressRepository.findAll(PageRequest.of(page, size));
        return new ApiResponse("All Address", addressPage.getContent().stream().map(this::getAddressDto).collect(Collectors.toList()));
    }

    public ApiResponse selectAddress(int R, Double lon, Double lat) {
        try {
            if (R <= 0) {
                return new ApiResponse("All locations",
                        addressRepository.findByActiveTrue());
            } else if (lon == null
                    || lat == null) {
                return new ApiResponse("Enter the correct location",
                        HttpStatus.CONFLICT.value());
            } else {
                return new ApiResponse("NearBy locations",
                        nearByLocations(R,
                                lon,
                                lat));
            }
        } catch (Exception e) {
            return new ApiResponse(e.getMessage(),
                    HttpStatus.CONFLICT.value());
        }
    }

    private List<Address> nearByLocations(int R, Double lon, Double lat) {
        double res = (0.009 * R);
        return addressRepository.findByLonBetweenAndLatBetween(
                lon - res,
                lon + res,
                lat - res,
                lat + res);
    }

    public ApiResponse deleteAddress(UUID id) {
        try {
            addressRepository.deleteById(id);
            return new ApiResponse("Deleted", HttpStatus.OK.getReasonPhrase());
        } catch (Exception e) {
            return new ApiResponse("Error for deleted", HttpStatus.CONFLICT.value());
        }
    }

    public AddressDto getAddressDto(Address address) {
        return new AddressDto(
                address.getId(),
                address.getName(),
                address.getLat(),
                address.getLon(),
                getDistrictDto(address.getDistrict()),
                address.isActive()
        );
    }

    public DistrictDto getDistrictDto(District district) {
        return new DistrictDto(
                district.getId(),
                district.getName(),
                getRegionDto(district.getRegion()),
                district.isActive()
        );
    }

    public RegionDto getRegionDto(Region region) {
        return new RegionDto(
                region.getId(),
                region.getName(),
                region.isActive()
        );
    }

}
