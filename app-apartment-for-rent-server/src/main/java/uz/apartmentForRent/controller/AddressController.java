package uz.apartmentForRent.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.apartmentForRent.payload.AddressDto;
import uz.apartmentForRent.payload.ApiResponse;
import uz.apartmentForRent.service.AddressService;

import java.util.UUID;

@RestController
@RequestMapping(value = "/api/address")
public class AddressController {

    @Autowired
    AddressService addressService;

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADIMIN')")
    @PostMapping
    public ApiResponse saveOrEditAddress(@RequestBody AddressDto dto) {
        return addressService.saveOrEditAddress(dto);
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADIMIN')")
    @GetMapping("/getAllAddress")
    public ApiResponse getAddressList(@RequestParam(value = "page", defaultValue = "0") int page,
                                      @RequestParam(value = "size", defaultValue = "10") int size) {
        return addressService.getAllAddress(page, size);
    }

    @GetMapping("/selectAddress")
    public ApiResponse selectAddress(@RequestParam int R,
                                     @RequestParam Double lon,
                                     @RequestParam Double lat){
        return addressService.selectAddress(R,lon,lat);
    }

    @PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADIMIN')")
    @DeleteMapping("/removeAddress/{id}")
    public ApiResponse deleteAddress(@PathVariable UUID id){
        return addressService.deleteAddress(id);
    }
}
