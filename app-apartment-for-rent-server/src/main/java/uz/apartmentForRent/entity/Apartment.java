package uz.apartmentForRent.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.apartmentForRent.entity.enums.StatusType;
import uz.apartmentForRent.entity.enums.Timing;
import uz.apartmentForRent.entity.template.UuidEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Apartment extends UuidEntity {

    @ManyToOne(optional = false)
    private User user;

    @OneToOne(optional = false)
    private Address address;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Attachment> photos;

    @Enumerated(EnumType.STRING)
    private StatusType statusType;

    @Enumerated(EnumType.STRING)
    private Timing timing;

    private int countRoom;//

    private Double sum;

    @ManyToOne
    private Category category;

    //private Double paySumForProgram;

    //private boolean pay;

    private String description;

    private boolean active;

    public Apartment(User user, Address address, List<Attachment> photos, StatusType statusType, Timing timing, int countRoom, Double sum, Category category, String description) {
        this.user = user;
        this.address = address;
        this.photos = photos;
        this.statusType = statusType;
        this.timing = timing;
        this.countRoom = countRoom;
        this.sum = sum;
        this.category = category;
        this.description = description;
    }
}
