package uz.apartmentForRent.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.apartmentForRent.entity.Category;

import java.util.UUID;

@Projection(name = "customCategory", types = Category.class)
public interface CustomCategory {
    UUID getId();

    String getName();

    boolean isActive();

    String getDescription();
}
